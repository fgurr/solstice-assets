/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import $ from 'jquery';
import 'jquery-match-height';
import getMembers from '../api/eclipsefdn.members';
import template from './templates/member.mustache';
import { SD_IMG, AP_IMG, AS_IMG } from './eclipsefdn.constants.js';

const SD_NAME = 'Strategic Members';
const AP_NAME = 'Contributing Members';
const AS_NAME = 'Associate Members';

const EclipseFdnMembersList = (async () => {
  const element = document.querySelector('.eclipsefdn-members-list');
  if (!element) {
    return;
  }

  let membersListArray = [
    { level: SD_NAME, members: [] },
    { level: AP_NAME, members: [] },
    { level: AS_NAME, members: [] },
  ];
  let url = 'https://membership.eclipse.org/api/organizations?pagesize=100';

  const isRandom = element.getAttribute('data-ml-sort') === 'random';
  const level = element.getAttribute('data-ml-level');
  const wg = element.getAttribute('data-ml-wg');

  const errHandler = (err) =>
    (element.innerHTML = `<div class="alert alert-danger" role="alert"><p><strong>Error ${err}</strong></p> <p>Sorry, something went wrong, please try again later.</p></div>`);

  let membersData = await getMembers(url, [], errHandler);

  if (level) {
    // Only display the members that are the levels specified in data-ml-level
    membersData = membersData.filter((member) =>
      member.levels.find((item) => level.toLowerCase().includes(item.level.toLowerCase()))
    );
  }

  if (wg) {
    // Only display the members that are the members of the working group specified in data-ml-wg
    membersData = membersData.filter((member) =>
      member.wgpas.find((item) => item.working_group.toLowerCase() === wg.toLowerCase())
    );

    // When we show members belong to a wg, no need to show EF levels
    membersListArray = [{ level: '', members: [] }];
  }

  const addMembersWithoutDuplicates = (levelName, memberData) => {
    // When we show members belong to a wg, only 1 item exits in membersListArray
    const currentLevelMembers = wg ? membersListArray[0] : membersListArray.find((item) => item.level === levelName);
    const isDuplicatedMember = currentLevelMembers.members.find(
      (item) => item.organization_id === memberData.organization_id
    );
    !isDuplicatedMember && currentLevelMembers.members.push(memberData);
  };

  membersData = membersData.map((member) => {
    if (!member.name) {
      // will not add members without a title/name into membersListArray to display
      return member;
    }
    if (member.levels.find((item) => item.level?.toUpperCase() === 'SD')) {
      if (!member.logos.web) {
        member.logos.web = SD_IMG;
      }
      addMembersWithoutDuplicates(SD_NAME, member);
      return member;
    }

    if (member.levels.find((item) => item.level?.toUpperCase() === 'AP' || item.level?.toUpperCase() === 'OHAP')) {
      if (!member.logos.web) {
        member.logos.web = AP_IMG;
      }
      addMembersWithoutDuplicates(AP_NAME, member);
      return member;
    }

    if (member.levels.find((item) => item.level?.toUpperCase() === 'AS')) {
      if (!member.logos.web) {
        member.logos.web = AS_IMG;
      }
      addMembersWithoutDuplicates(AS_NAME, member);
      return member;
    }
    return member;
  });

  if (isRandom) {
    // Sort randomly
    membersListArray.forEach((eachLevel) => {
      let tempArray = eachLevel.members.map((item) => item);
      const randomItems = [];
      eachLevel.members.forEach(() => {
        const randomIndex = Math.floor(Math.random() * tempArray.length);
        randomItems.push(tempArray[randomIndex]);
        tempArray.splice(randomIndex, 1);
      });
      eachLevel.members = randomItems;
    });
  } else {
    // Sort alphabetically by default
    membersListArray.forEach((eachLevel) => {
      eachLevel.members.sort((a, b) => {
        const preName = a.name.toUpperCase();
        const nextName = b.name.toUpperCase();
        if (preName < nextName) {
          return -1;
        }
        if (preName > nextName) {
          return 1;
        }
        return 0;
      });
    });
  }

  if (level) {
    membersListArray = membersListArray.filter((list) => list.members.length !== 0);
  }

  element.innerHTML = template({
    sections: membersListArray,
    hostname: window.location.hostname.includes('staging.eclipse.org')
      ? 'https://staging.eclipse.org'
      : 'https://www.eclipse.org',
  });
  $.fn.matchHeight._applyDataApi();
})();
export default EclipseFdnMembersList;
