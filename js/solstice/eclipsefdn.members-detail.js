/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou fang <zhou.fang@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import getMembers from '../api/eclipsefdn.members';
import template from './templates/member-detail.mustache';
import { SD_IMG, AP_IMG, AS_IMG } from './eclipsefdn.constants.js';

const EclipseFdnMemberDetail = (async () => {
  const element = document.querySelector('.member-detail');
  if (!element) {
    return;
  }

  const orgId = new URL(window.location.href).searchParams.get('member_id');

  if (!orgId) {
    // Show error message if member_id is missing
    element.innerHTML =
      '<div class="alert alert-danger" role="alert">Error: Sorry, please make sure "member_id" exists in the URL and equals to a valid number.</div>';
    return;
  }

  if (!Number(orgId)) {
    // Show error message if member_id is not a valid number
    element.innerHTML =
      '<div class="alert alert-danger" role="alert">Error: Sorry, please make sure "member_id" equals to a valid number.</div>';
    return;
  }

  const errHandler = (err) => {
    element.innerHTML = `<div class="alert alert-danger" role="alert"><p><strong>Error ${err}</strong></p> <p>Sorry, something went wrong, please try again later.</p></div>`;
  };

  let pool = [
    getMembers(`https://membership.eclipse.org/api/organizations/${orgId}`, [], errHandler),
    getMembers(`https://membership.eclipse.org/api/organizations/${orgId}/projects`, [], errHandler),
    getMembers(`https://membership.eclipse.org/api/organizations/${orgId}/products`, [], errHandler),
  ];
  let memberDetailData = await Promise.all(pool);
  memberDetailData = { ...memberDetailData[0][0], projects: memberDetailData[1], products: memberDetailData[2] };

  const getMemberLevelImg = function () {
    const levels = this.levels;
    if (levels.find((item) => item.level?.toUpperCase() === 'SD')) {
      return SD_IMG;
    }
    if (levels.find((item) => item.level?.toUpperCase() === 'AP' || item.level?.toUpperCase() === 'OHAP')) {
      return AP_IMG;
    }
    if (levels.find((item) => item.level?.toUpperCase() === 'AS')) {
      return AS_IMG;
    }
  };

  const shouldShowLinksSideBar = function () {
    if (this.products?.length > 0 || this.projects?.length > 0) {
      return true;
    }
    return false;
  };

  element.innerHTML = template({
    section: memberDetailData,
    getMemberLevelImg,
    shouldShowLinksSideBar,
    trimDescription: function () {
      return this.description.long.replaceAll('\\r\\n', '').replaceAll('\\', '');
    },
  });
})();

export default EclipseFdnMemberDetail;
