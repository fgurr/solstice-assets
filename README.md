# solstice-assets

Assets for the Eclipse Foundation look and feel called Solstice.

## Getting started

Install dependencies, build assets and watch for changes:

```bash
yarn && yarn run drupal && yarn run watch
```

## How to use this project

Make sure to require [./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets/-/raw/master/webpack-solstice-assets.mix.js) in you webpack.mix.js to load our default configurations. You are likely to run into issues building your project without it.

You also need to call `mix.EclipseFdnSolsticeAssets()` to load them.

```js
require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();
```

We strongly recommend adding the following to your project package.json file in order to support IE11. However, we plan to drop IE11 support on [June 15, 2022](https://docs.microsoft.com/en-us/lifecycle/faq/internet-explorer-microsoft-edge).

```json
"browserslist": "last 5 version, > 0.2%, not dead, IE 11"
```

For more information, you can take a look at the [browserslist](https://www.npmjs.com/package/browserslist) module. Note that some version of babel does not support [loading the config from package.json](https://vuejs-templates.github.io/webpack/babel.html). To address this, we recommend creating a file named ``babel.config.json`` and copy the following content in that file:

```json
{
  "extends": "./node_modules/eclipsefdn-solstice-assets/babel.config.json"
}
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets/) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_github_username]/solstice-assets.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Maintainers

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

**Eric Poirier (Eclipse Foundation)**

- <https://twitter.com/@ericpoir>
- <https://github.com/ericpoirier>

## Browser Support

We transpile our code via the Babel plugin to ensure compatibility with older browsers where possible using polyfills. We target support for the latest 5 versions of the following browsers:

- Firefox
- Chrome
- Opera (mini and standard)
- Safari (iOS and desktop)
- Edge

In addition, we also support the following browser versions:

- IE 11
- KaiOS 2.5
- Opera Mobile 59
- Baidu 7.12
- Latest Android browser versions (Chrome, Firefox, QQ, UC, and base Android)

In development, we test in modern browsers to ensure the general use-case is met and make best efforts to fix any issues that arise with supported browsers.

## Trademarks

* Jakarta and Jakarta EE are Trademarks of the Eclipse Foundation, Inc.
* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2018-2021 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [solstice-assets authors](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets/-/graphs/master). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets/-/raw/master/README.md).
